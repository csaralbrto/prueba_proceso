<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Tu Contraseña a sido cambiada!!',
    'sent' => 'Te Hemos enviado un link a tu correo!',
    'token' => 'El token de cambio de contraseña es invalido',
    'user' => "No pudimos encontrar un usuario con ese email.",

];
