@extends('layouts.main')

@section('headers_style')
    <link rel="stylesheet" type="text/css" href="css/toastr.min.css">
@stop

@section('content')
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-sm-12 col-md-6 col-lg-6">
             <div class="card row top-3">
              <h5 class="card-header">Cambiar Contraseña</h5>
              <div class="card-body">
                    <form id="form_create" method="post"  enctype="multipart/form-data">
                         <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                          <div class="form-group row">
                            <label for="inputPassword" class="col-lg-4 col-md-4 col-form-label">Contraseña</label>
                            <div class="col-lg-5 col-md-7 col-sm-12">
                              <input type="password" name="password" class="form-control" id="password" placeholder="Contraseña" required>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputPassword" class="col-lg-4 col-md-4 col-form-label">Confirmar Contraseña</label>
                            <div class="col-lg-5 col-md-7">
                              <input type="password"  class="form-control" name="password_confirm" id="password_confirm" placeholder="Repita Contraseña" required>
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="col-md-4"></div>
                            <div class="col-md-6">
                                <button type="reset" class="btn btn-danger">
                                    Cancelar
                                </button>
                                <button type="submit" class="btn btn-primary">
                                    Guardar
                                </button>
                            </div>
                          </div>
                    </form>
                </div>
            </div>
        </div>
     </div>
@stop

@section('footer_scripts')
    <script type="text/javascript" src="js/toastr.min.js"></script>

    <script type="text/javascript">
        /*Envio del formulario*/
        $('#form_create').submit(function(event){
            event.preventDefault();
            console.log("Entro aqui");
            var formdata = new FormData($("#form_create")[0]);
            /*Validación de la contraseña*/
            var password = $("#password").val();
            var passwordconfirm = $("#password_confirm").val();

            if (password!=passwordconfirm) {
                toastr.warning("Contraseñas no coinciden");
                return
            }
            $.ajax({
                url: '/ChangePassword',
                type: 'POST',
                data: formdata,
                cache:false,
                contentType:false,
                processData:false,
                success: function (data) {
                    toastr.success("Guardado exitosamente");
                    console.log(data);
                },
                error: function (error) {
                    if (error.status==409) {
                        var message_error = JSON.parse(error.responseText);
                        toastr.warning(message_error.message);
                    } else {
                        toastr.error("Error de servidor");
                    }
                }
            });
        });

    </script>

@stop
