@extends('layouts.main')

@section('headers_style')
   
@stop

@section('content')
<div class="container">
    <div class="row top-3"> 
        <div class="col-md-2"></div>
        <div class="col-md-8">
             <div class="card">
              <h5 class="card-header">Olvido de contraseña</h5>
              <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form id="form_forgot" method="POST" action="{{ route('password.email')}}">
                         <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                          <div class="form-group row">
                            <label for="staticEmail" class="col-sm-3 col-form-label">Email <span class="color-red">*</span></label>
                            <div class="col-sm-8">
                              <input type="email" name="email" class="form-control" id="staticEmail" placeholder="email@example.com" required>
                              @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                              @endif
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="col-md-6">
                              <p>(<span class="color-red">*</span>) Campos requeridos</p>
                            </div>
                            <div class="col-md-6">
                                <button type="reset" class="btn btn-danger">
                                    Cancelar
                                </button>
                                <button type="submit" class="btn btn-primary">
                                    Enviar
                                </button>
                            </div>
                          </div>
                    </form>
                </div>
            </div>
        </div>
     </div>
</div>
@stop


@section('footer_scripts')
@stop