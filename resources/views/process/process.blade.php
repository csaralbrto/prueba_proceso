@extends('layouts.main')

@section('headers_style')
	<link rel="stylesheet" type="text/css" href="vendors/dataTables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="css/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/jquery-confirm.css')}}">
@stop


@section('content')
<!--Vista principal de los procesos-->
	<div class="container">
		<div class="card top-3">
			<div class="card-header">
    			Procesos
    			<input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
    			<label value="{{$process_number= config('constants.keys.process_number')}}"></label>
  			</div>
  			<div class="card-body">
	  			<div class="row top2">
					<div class="col-md-10"></div>
					<div class="col-md-2">
						<button type="button" title="Crear Proceso" id="btn_crear" data-toggle="modal" data-target="#ModalCreate" class="btn btn-primary">Crear</button>
					</div>
				</div>
				<div class="row top2">
				    <div class="col-md-12">
				        <div class="panel-body">
				            <div class="table-responsive">
				            <table class="table table-bordered " id="table1">
				                <thead>
				                <tr class="filters">
				                    <th>Numero de Proceso</th>
				                    <th>Descripcion</th>
				                    <th>Fecha de Creación</th>
				                    <th>Opciones</th>
				                </tr>
				                </thead>
				                <tbody>
				                </tbody>
				            </table>
				            </div>
				        </div>
				    </div>
				</div>
  			</div>
		</div>
	</div>

		<!--Modal crear proceso-->
	<div class="modal fade" id="ModalCreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <form id="form_create" method="post"  enctype="multipart/form-data">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Nuevo Proceso</h5>
		        <button type="button" id="create_close" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row"> 
		        	<input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
		        	<input type="hidden" id="numprocess" name="numprocess" value="{{ $process_number }}">
		        	<div class="col-md-6">
		        		<label>Descripcion <span class="required">*</span></label>
		        		<textarea class="form-control" name="description" id="create_description" rows="4" cols="200" maxlength="200"></textarea required>
		        	</div>
		        	<div class="col-md-6">
		        		<label>Sede <span class="required">*</span></label>
		        		<select class="form-control" name="site" id="create_Country" required>
						    <option value="Colombia">Colombia</option>
						    <option value="México">México</option>
						    <option value="Perú">Perú</option>
						</select>
		        	</div>
		        </div>
		        <div class="row">
		        	<div class="col-md-12">
		        		<label>Presupuesto COP <span class="required">*</span></label>
		        		<input class="form-control" type="number" name="budget" id="create_budget" value="0" step=".01" required>
		        	</div>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <span>Los campos con <span class="required">*</span> son requeridos</span><button type="submit" class="btn btn-primary">Guardar</button>
		      </div>
	      </form>
	    </div>
	  </div>
	</div>

	<!--Modal de ver Proceso-->
	<div class="modal fade" id="ModalSee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <form id="form_see" method="post"  enctype="multipart/form-data">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Ver Proceso</h5>
		        <button type="button" id="create_close" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		      	 <div class="row">
		        	<input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
		        	<div class="col-md-6">
		        		<label>Numero de Proceso</label>
		        		<input class="form-control readonly" type="text" name="budget" id="see_numprocess" readonly required>
		        	</div>
		        </div>
		        <div class="row"> 
		        	<div class="col-md-6">
		        		<label>Descripcion</label>
		        		<textarea class="form-control" name="description" id="see_description" rows="4" cols="200" readonly></textarea>
		        	</div>
		        </div>
		        <div class="row"> 
		        	<div class="col-md-6">
		        		<label>Sede</label>
		        		<input class="form-control readonly" type="text" name="description" id="see_Country" readonly required>
		        	</div>
		        </div>
		        <div class="row">
		        	<div class="col-md-6">
		        		<label>Presupuesto COP</label>
		        		<input class="form-control readonly" type="number" name="budget" id="see_budget" readonly required>
		        	</div>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" id="create_close" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">Cerrar</span>
		        </button>
		      </div>
	      </form>
	    </div>
	  </div>
	</div>

	<!--Modal de editar Proceso-->
	<div class="modal fade" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <form id="form_update" method="post"  enctype="multipart/form-data">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Actualizar Proceso</h5>
		        <button type="button" id="create_close" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row"> 
		        	<input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
		        	<input type="hidden" id="update_id" name="process_id">
		        	<input type="hidden" id="update_numprocess" name="numprocess">
		        	<div class="col-md-6">
		        		<label>Descripcion <span class="required">*</span></label>
		        		<textarea class="form-control" name="description" id="update_description" rows="4" cols="200" maxlength="200"></textarea required>
		        	</div>
		        	<div class="col-md-6">
		        		<label>Sede <span class="required">*</span></label>
		        		<select class="form-control" name="site" id="update_Country" required>
						    <option value="Colombia">Colombia</option>
						    <option value="México">México</option>
						    <option value="Perú">Perú</option>
						</select>
		        	</div>
		        </div>
		        <div class="row">
		        	<div class="col-md-12">
		        		<label>Presupuesto COP<span class="required">*</span></label>
		        		<input class="form-control" type="number" name="budget" id="update_budget" required>
		        	</div>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <span>Los campos con <span class="required">*</span> son requeridos</span><button type="submit" class="btn btn-primary">Guardar</button>
		        <button type="button" id="create_close" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">Cerrar</span>
		        </button>
		      </div>
	      </form>
	    </div>
	  </div>
	</div>
@stop


@section('footer_scripts')
	<script type="text/javascript" src="vendors/dataTables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/toastr.min.js"></script>
    <script type="text/javascript" src="{{ asset ('js/jquery-confirm.js')}}"></script>

<!--Aqui se carga la tabla que muestra los procesos y las operaciones de estos, mediante ajax-->
    <script type="text/javascript">
      var table;
	  var idioma = {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }};


         load();

        /*carga la 1era tabla*/
        function load () {
        	$(function() {
	            table = $('#table1').DataTable({
	                processing: true,
	                serverSide: true,
	                ajax: '{!! route('process_datatable') !!}',
	                columns: [
	                    { data: 'numprocess', name:'numprocess'},
	                    { data: 'description', name: 'description' },
	                    { data: 'created_at', name: 'created_at' },
	                    { defaultContent : "<button type='button' title='Ver Proceso' data-toggle='modal' data-target='#ModalSee' class='verProceso btn btn-success'><span class='fa fa-eye'></span></button><button type='button' title='Editar Proceso' data-toggle='modal' data-target='#ModalUpdate' class='EditarProceso btn btn-secondary'><span class='fa fa-pencil'></span></button><button type='button' title='Eliminar Proceso' class='EliminarProceso btn btn-danger'><span class='fa fa-trash'></span></button>"}
	                ],
	                language: idioma
	            });
	            table.on( 'draw', function () {
	                $('.livicon').each(function(){
	                    $(this).updateLivicon();
	                });
	            } );
	            obtenerData("#table1 tbody",table);
	        });
        }
        	/*Obtiene la data de una linea*/
	        function obtenerData (tbody, table) {
	            console.log(table);
	            /*Funcion para cargar los datos dentro de la vista de ver proceso*/
	            $(tbody).on("click", "button.verProceso", function(){
	                var data = table.row($(this).parents("tr")).data();
	                console.log(data.edition);
	                $('#see_description').val(data.description);
	                $('#see_Country').val(data.site);
	                $('#see_budget').val(data.budget);
	                $('#see_numprocess').val(data.numprocess);
	                $('#see_user').val(data.id_user);
	            });

	            /*Funcion para cargar los datos dentro de la vista de actualizar proceso*/
	            $(tbody).on("click", "button.EditarProceso", function(){
	                var data = table.row($(this).parents("tr")).data();
	                console.log(data.edition);
	                $('#update_numprocess').val(data.numprocess);
	                $('#update_description').val(data.description);
	                $('#update_Country').val(data.site);
	                $('#update_budget').val(data.budget);
	                $('#update_id').val(data.id);
	            });
	            /*Funcion del boton para eliminar proceso*/
	             $(tbody).on("click", "button.EliminarProceso", function(){
	                var data = table.row($(this).parents("tr")).data();
	                confirmDelete(data.id, data.description);
	            });
	        }


	            	/*confirmar si quiere de verdad eliminar el proceso*/
    	function confirmDelete(process_id, process_description) {
    		console.log("Proceso a eliminar ", process_id);
			  $.confirm({
			    title: 'Eliminar: '+process_description,
			    content: '¿Realmente desea eliminar el Proceso?',
			    buttons: {
			        Eliminar: {
			            text: 'Eliminar',
			            btnClass: 'btn-red',
			            keys: ['enter', 'shift'],
			            action: function(){
			                deleteProcess(process_id);
			            }
			        },
			        Cancelar: {
			        	text: 'Cancelar',
			        	btnClass: 'btn-blue',
			        	keys: ['enter', 'shift'],
			            action: function(){
			              
			            }
			        }
			    }
			});
    	}
    	/*Eliminar el proceso*/
    	function deleteProcess (process_id) {
    		var token = $('#_token').val();
    		var data = {'process_id': process_id};
    		data = JSON.stringify(data);
    		$.ajax({
				url: '/delete_process',
				type: 'POST',
				data: data,
				cache:false,
				contentType:'application/json',
				processData:false,
				headers: {'X-CSRF-TOKEN': token},
				success: function (data) {
					toastr.success("Eliminado Exitosamente");
					console.log(data)
					reload();
				},
				error: function (error) {
					if (error.status==409) {
						var message_error = JSON.parse(error.responseText);
						toastr.warning(message_error.message);
					} else {
						toastr.error("Error de servidor");
					}
				}
			});
    	}
	        /*Formulario para crear proceso*/
		    $('#form_create').submit(function(event){
				event.preventDefault();
				var formdata = new FormData($("#form_create")[0]);
				$.ajax({
					url: '/process',
					type: 'POST',
					data: formdata,
					cache:false,
					contentType:false,
					processData:false,
					success: function (data) {
						console.log(data);
						toastr.success("Guardado exitosamente");
						$('#form_create')[0].reset();
						location.reload();
					},
					error: function (error) {
						if (error.status==409) {
							var message_error = JSON.parse(error.responseText);
							toastr.warning(message_error.message);
						} else {
							toastr.error("Error de servidor");
						}
					}
				});
			});


	        /*Formulario para actualizar proceso*/
		    $('#form_update').submit(function(event){
				event.preventDefault();
				var formdata = new FormData($("#form_update")[0]);
				$.ajax({
					url: '/update_process',
					type: 'POST',
					data: formdata,
					cache:false,
					contentType:false,
					processData:false,
					success: function (data) {
						console.log(data);
						toastr.success("Actualizado exitosamente");
						reload();
					},
					error: function (error) {
						if (error.status==409) {
							var message_error = JSON.parse(error.responseText);
							toastr.warning(message_error.message);
						} else {
							toastr.error("Error de servidor");
						}
					}
				});
			});
		    /*refrescar la tabla*/
	        function reload () {
	        	$('#table1').DataTable().ajax.reload();
	        }



    </script>
@stop
