<!--Footer-->
<footer class="page-footer font-small stylish-color-dark pt-4 mt-4">
      
      <!--Footer Links-->
      <div class="container text-center text-md-left">
          <div class="row">
  
              <!--First column-->
              <div class="col-md-4">
                  <h5 class="text-uppercase mb-4 mt-3 font-weight-bold">Revista Escala</h5>
                    <hr class="rgba-white-light" style="width: 60px;">
                  <p>Here you can use rows and columns here to organize your footer content. Lorem ipsum dolor sit
                      amet, consectetur adipisicing elit.</p>
              </div>
              <!--/.First column-->
  
              <hr class="clearfix w-100 d-md-none">
  
              <!--Second column-->
              <div class="col-md-2 mx-auto">
                  <h5 class="text-uppercase mb-4 mt-3 font-weight-bold">Links</h5>
                  <ul class="list-unstyled">
                      <li><a href="#!">Link 1</a></li>
                      <li><a href="#!">Link 2</a></li>
                      <li><a href="#!">Link 3</a></li>
                      <li><a href="#!">Link 4</a></li>
                  </ul>
              </div>
              <!--/.Second column-->
  
              <hr class="clearfix w-100 d-md-none">
  
              <!--Third column-->
              <div class="col-md-2 mx-auto">
                  <h5 class="text-uppercase mb-4 mt-3 font-weight-bold">Links</h5>
                  <ul class="list-unstyled">
                      <li><a href="#!">Link 1</a></li>
                      <li><a href="#!">Link 2</a></li>
                      <li><a href="#!">Link 3</a></li>
                      <li><a href="#!">Link 4</a></li>
                  </ul>
              </div>
              <!--/.Third column-->
  
              <hr class="clearfix w-100 d-md-none">
  
              <!--Fourth column-->
                <div class="col-md-4 col-lg-3 col-xl-3">
                    <h5 class="text-uppercase font-weight-bold"><strong>Contactanos</strong></h5>
                    <hr class="rgba-white-light" style="width: 60px;">
                    <p><i class="far fa-clock mr-3"></i> Lunes – Viernes | 9am – 5pm</p>
                    <p><i class="far fa-map-marker-alt mr-3"></i>Carrera 22 No. 63-81, Bogotá</p>
                    <p><i class="fa fa-envelope mr-3"></i> info@revistaescala.com</p>
                    <p><i class="fa fa-phone mr-3"></i> + 57 (1) 6617297</p>
                    <p><i class="fab fa-whatsapp mr-3"></i> + 57 322 425 3028</p>
                </div>
              <!--/.Fourth column-->
          </div>
      </div>
      <!--/.Footer Links-->
  
      <hr>
  
      <!--Call to action-->
      <div class="text-center py-3">
          <ul class="list-unstyled list-inline mb-0">
              <li class="list-inline-item">
                  <h5 class="mb-1">Suscribete</h5>
              </li>
              <li class="list-inline-item"><a href="#!" class="btn btn-danger btn-rounded">Suscribete!</a></li>
          </ul>
      </div>
      <!--/.Call to action-->
  
      <hr>
  
      <!--Social buttons-->
      <div class="text-center">
            <h5 class="text-uppercase font-weight-bold"><strong>Siguenos</strong></h5>
            <hr class="rgba-white-light" style="width: 60px;">
          <ul class="list-unstyled list-inline">
              <li class="list-inline-item"><a href="https://www.facebook.com/revistaescala/" class="btn-floating btn-sm btn-fb mx-1"><i class="fa fa-facebook"> </i></a></li>
              <li class="list-inline-item"><a href="https://twitter.com/revistaescala?lang=es" class="btn-floating btn-sm btn-tw mx-1"><i class="fa fa-twitter"> </i></a></li>
              <li class="list-inline-item"><a href="https://www.instagram.com/revistaescala/" class="btn-floating btn-sm btn-li mx-1"><i class="fa fa-instagram"> </i></a></li>
          </ul>
      </div>
      <!--/.Social buttons-->
  
      <!--Copyright-->
      <div class="footer-copyright py-3 text-center">
          <div class="container-fluid">
             Hecho con ♥ por <a href="http://www.difference.com.co/">Difference </a>
  
          </div>
      </div>
      <!--/.Copyright-->
  
  </footer>
  <!--/.Footer-->