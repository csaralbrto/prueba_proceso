<!DOCTYPE html>
<html>
<head>
		    
	<title>Procesos</title>
	<link rel="icon" type="text/css"  href="{{ asset('images/d.ico') }}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/evclass.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.css')}}">
	@yield('headers_style')
	 
	<script type="text/javascript" src="{{ asset('js/jquery-3.2.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/main.js')}}"></script>
	<script>
	</script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light background-DarkRed">
	  <div class="container">
	  <button class="navbar-toggler color-back-white" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon color-white"></span>
	  </button>

	  <div class="top-3 collapse navbar-collapse color-white" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto navbar-left">
	      <li class="nav-item active">
		  <a class="nav-link color-white" href="{{ route('main') }}"><i class="border-icon fa fa-home mr-2" aria-hidden="true"></i>Home <span class="sr-only">(current)</span></a>
	      </li>
	      @auth
	       @if (Auth::user()->role_id == 1)
		      <li class="nav-item dropdown">
		      	<a class="nav-link dropdown-toggle color-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
			         <span class="color-white"><i class="border-icon fa fa-cogs mr-2" aria-hidden="true"></i>Administrar</span>
			         <span class="caret"></span>
			     </a>
			     <div class="dropdown-menu" aria-labelledby="navbarDropdown">
			     	<a class="dropdown-item" href="{{ route('process') }}">Procesos <span class="sr-only">(current)</span></a>				 
			     </div>
		      </li>
		    @endif
	      @endauth
	    </ul>
	    <ul class="nav navbar-nav navbar-right">
	    @auth
		      <li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle color-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		         
				 <i class="fa fa-fw fa-user color-white mr-2" aria-hidden="true"></i>
				
		         <span class="color-white">{{ Auth::user()->name }} </span>
		         <span class="caret"></span>
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		          <a class="dropdown-item"  
		            href="{{ route('ChangePassword') }}">
		           <span class='fa fa-key mr-2'></span> Cambiar Contraseña
		          </a>
		          <a class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" 
		            href="{{ route('logout') }}">
		           <span class='fa fa-fw fa-power-off mr-2'></span> Salir
		          </a>
		          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                   {{ csrf_field() }}
	              </form>
		         </div>
			   </li>
		   @else 
		      <li class="nav-item botones">
			  		<a class="nav-link color-botones" href="{{ route('RegisterUser') }}"><i class="botones fa fa-pencil-square-o mr-2"></i>Registrarse <span class="sr-only">(current)</span></a>
		      </li>
			  <br> 
		      <li class="nav-item botones">
			 	 	 <a class="nav-link color-botones" href="{{ route('login') }}"><i class="botones fa fa-sign-in mr-2"></i>Iniciar Sesión <span class="sr-only">(current)</span></a>
			  </li>
	     @endauth
		 </ul>
	  </div>
	  </div>
	</nav>
	@yield('content')
	@yield('footer_scripts')
</body>



<footer class="page-footer font-small stylish-color-dark pt-5 mt-5 background-DarkRed">
      <div style="background-color: #1c1c1c;" class="footer-copyright py-3 text-center color-white">
          <div class="container-fluid">
             Ing. Cesar Barriosnuevos
          </div>
      </div>  
</footer>


</html>