@extends('layouts.main')

@section('headers_style')
   
@stop

@section('content')
<div class="container">
    <div class="row top-3"> 
        <div class="col-md-2"></div>
        <div class="col-md-8">
             <div class="card">
              <h5 class="card-header">Resetear Contraseña</h5>
              <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form id="form_forgot" method="POST" action="{{ route('password.request')}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="token" value="{{ $token }}">
                          <div class="form-group row">
                            <label for="staticEmail" class="col-sm-3 col-form-label">Email<span class="color-red">*</span></label>
                            <div class="col-sm-7">
                              <input type="email" name="email" class="form-control" id="staticEmail" placeholder="email@example.com" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputPassword" class="col-sm-3 col-form-label">Contraseña <span class="color-red">*</span></label>
                            <div class="col-sm-7">
                              <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputPassword" class="col-sm-3 col-form-label">Confirmar Contraseña <span class="color-red">*</span></label>
                            <div class="col-sm-7">
                              <input type="password"  class="form-control" name="password_confirmation" id="password_confirm" placeholder="Repita Password" required>
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                          </div>

                        <div class="form-group row">
                          <div class="col-md-4">
                            <p>(<span class="color-red">*</span>) Campos requeridos</p>
                          </div>
                          <div class="col-md-6">
                              <button type="submit" class="btn btn-primary">
                                  Cambiar Contraseña
                              </button>
                          </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
     </div>
</div>
@stop


@section('footer_scripts')
@stop