@extends('layouts.main')

@section('headers_style')
    <link rel="stylesheet" type="text/css" href="css/toastr.min.css">
@stop

@section('content')
    <div class="row"> 
        <div class="col-md-3"></div>
        <div class="col-lg-7 col-md-6 col-sm-12">
             <div class="card row top-3">
              <h5 class="card-header">Registro</h5>
              <div class="card-body">
                    <form id="form_create" method="post"  enctype="multipart/form-data">
                         <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                         <div class="form-group row">
                            <label for="inputName" class="col-lg-4 col-md-12 col-form-label">Nombre<span class="color-red">*</span></label>
                            <div class="col-lg-7 col-md-12">
                              <input type="text" name="name" class="form-control" id="Name" required>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputName" class="col-lg-4 col-md-12 col-form-label">Teléfono <span class="color-red">*</span></label>
                            <div class="col-lg-7 col-md-12">
                              <input type="text" name="phone" class="form-control" id="phone" placeholder="+573005215847" required>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-lg-4 col-md-12 col-form-label">Email <span class="color-red">*</span></label>
                            <div class="col-lg-7 col-md-12">
                              <input type="email" name="email" class="form-control" id="staticEmail" placeholder="email@example.com" required>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputPassword" class="col-lg-4  col-md-12 col-form-label">Contraseña <span class="color-red">*</span></label>
                            <div class="col-lg-7 col-md-10">
                              <input type="password" name="password" class="form-control" id="password" placeholder="Contraseña" required>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputPassword" class="col-lg-4 col-md-12 col-form-label">Confirmar Contraseña <span class="color-red">*</span></label>
                            <div class="col-lg-7 col-md-12">
                              <input type="password"  class="form-control" name="password_confirm" id="password_confirm" placeholder="Repita Contraseña" required>
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="col-md-4">
                               <p>(<span class="color-red">*</span>) Campos requeridos</p>
                            </div>
                            <div class="col-md-8">
                                <button type="reset" class="btn btn-danger">
                                    Cancelar
                                </button>
                                <button type="submit" class="btn btn-primary">
                                    Registrarse
                                </button>
                            </div>
                          </div>
                    </form>
                </div>
            </div>
        </div>
     </div>
@stop

@section('footer_scripts')
    <script type="text/javascript" src="js/toastr.min.js"></script>

    <script type="text/javascript">
        /*Envio del formulario*/
        $('#form_create').submit(function(event){
            event.preventDefault();
            console.log("Entro aqui");
            var formdata = new FormData($("#form_create")[0]);
            /*Validación de la contraseña*/
            var password = $("#password").val();
            var passwordconfirm = $("#password_confirm").val();

            if (password!=passwordconfirm) {
                toastr.warning("Contraseñas no coinciden");
                return
            }
            $.ajax({
                url: '/RegisterUser',
                type: 'POST',
                data: formdata,
                cache:false,
                contentType:false,
                processData:false,
                success: function (data) {
                    //$('#form_create')[0].reset();
                    console.log(data);
                    toastr.success("Guardado exitosamente");
                },
                error: function (error) {
                    if (error.status==409) {
                        var message_error = JSON.parse(error.responseText);
                        toastr.warning(message_error.message);
                    } else {
                        toastr.error("Error de servidor");
                    }
                }
            });
        });

    </script>

@stop
