@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-lg-6 col-md-6">
             <div class="card row top-3">
              <h5 class="card-header">Iniciar Sesión</h5>
              <div class="card-body">
                    <form id="form_create" method="post"  enctype="multipart/form-data" action="{{ route('login2') }}">
                         <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                          <div class="form-group row">
                            <label for="staticEmail" class="col-lg-3 col-md-4 col-form-label">Email <span class="color-red">*</span></label>
                            <div class="col-lg-7 col-md-7">
                              <input type="email" name="email" class="form-control" id="staticEmail" placeholder="email@example.com" required>
                              @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                              @endif
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputPassword" class="col-lg-3 col-md-4 col-form-label">Contraseña <span class="color-red">*</span></label>
                            <div class="col-lg-7 col-md-7">
                              <input type="password" name="password" class="form-control" id="password" placeholder="Contraseña" required>
                              @if (session('status'))
                                  <span class="help-block">
                                        <strong>{{ session('status') }}</strong>
                                    </span>
                              @endif
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="col-md-5">
                              <p>(<span class="color-red">*</span>) Campos requeridos</p>
                            </div>
                            <div class="col-md-5">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>
                            </div>
                          </div>
                          <div class="row"> 
                            <div class="col-md-4"></div>
                            <div class="col-md-6">
                              <a href="{{ route('forgotPassword') }}">¿Olvidaste tu contraseña?</a>
                            </div>
                          </div>
                    </form>
                    <!-- </div>
                    <div class="modal footer">
                    <button type="button" class="btn btn-danger m-t-10" data-dismiss="modal">  Close</button>
                    </div>
                    </div>
                    </div>
                    </div> -->
                </div>
            </div>
        </div>
     </div>
@endsection
