@extends('layouts.main')

@section('headers_style')
   
@stop

@section('content')
<div class="container">
    <div class="row"> 
        <div class="col-md-2"></div>
        <div class="col-md-8">
             <div class="card">
              <h5 class="card-header">Olvido de contraseña</h5>
              <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form id="form_forgot" method="POST" action="{{ route('password.email')}}">
                         <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                          <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                              <input type="email" name="email" class="form-control" id="staticEmail" placeholder="email@example.com" required>
                              @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                              @endif
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">
                                    Enviar
                                </button>
                            </div>
                          </div>
                    </form>
                </div>
            </div>
        </div>
     </div>
</div>
@stop


@section('footer_scripts')
@stop