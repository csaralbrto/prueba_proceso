<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProviderImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provider_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description')->nullable();
            $table->string('picture_url')->nullable();
            $table->integer('provider_id')->unsigned();
            $table->foreign('provider_id')->references('id')->on('provider')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provider_images');
    }
}
