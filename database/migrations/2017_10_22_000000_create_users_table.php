<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('surname')->nullable();
            $table->string('email',128)->unique();
            $table->string('phone')->nullable();
            $table->boolean('isSuscribed')->default(false);
            $table->dateTime('EndSuscribed')->nullable();
            $table->string('country_code',128)->nullable();
            $table->integer('city_code')->unsigned()->nullable();
            $table->string('password');
            $table->string('picture_url')->nullable();
            $table->string('status')->default("1");
            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('role');
            $table->foreign('country_code')->references('code')->on('country');
            $table->foreign('city_code')->references('id')->on('city');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
