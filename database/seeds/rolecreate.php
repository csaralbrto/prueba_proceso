<?php

use Illuminate\Database\Seeder;

class rolecreate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Roles = [
        	['name'=> 'Admin'],
        	['name'=> 'User']
        ];

        foreach ($Roles as $rol) {
            \App\Role::create($rol);
        }
    }
}
