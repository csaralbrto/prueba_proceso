<?php

use Illuminate\Database\Seeder;

class usercreate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
          [
            'name'=>'Admin',
            'password'=>Hash::make('3sc4l42018'), 
            'email'=>'admin@admin.com',
            'role_id'=> '1',
            'isSuscribed'=> true,
            'EndSuscribed'=> '2200-03-14 09:13:51'
          ]
        ];


        foreach ($users as $user) {
            \App\User::create($user);
        }
    }
}
