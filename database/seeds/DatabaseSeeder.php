<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(rolecreate::class);
    	$this->call(usercreate::class);
        $this->call(CountrySeeder::class);
        $this->call(CitySeeder::class);
    }
}
