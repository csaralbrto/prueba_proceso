<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Countrys = [
            //América:
        	['name'=>'Antigua y Barbuda','code'=>'AG'],
            ['name'=>'Argentina','code'=>'AR'],
            ['name'=>'Bahamas','code'=>'BS'],
            ['name'=>'Barbados','code'=>'BB'],
            ['name'=>'Belice','code'=>'BZ'],
            ['name'=>'Bolivia','code'=>'BO'],
            ['name'=>'Brasil','code'=>'BR'],
            ['name'=>'Canadá','code'=>'CA'],
            ['name'=>'Chile','code'=>'CL'],
            ['name'=>'Colombia','code'=>'CO'],
            ['name'=>'Costa Rica','code'=>'CR'],
            ['name'=>'Cuba','code'=>'CU'],
            ['name'=>'Dominica','code'=>'DM'],
            ['name'=>'Ecuador','code'=>'EC'],
            ['name'=>'El Salvador','code'=>'SV'],
            ['name'=>'Estados Unidos','code'=>'US'],
            ['name'=>'Granada','code'=>'GD'],
            ['name'=>'Guatemala','code'=>'GT'],
            ['name'=>'Guyana','code'=>'GY'],
            ['name'=>'Haití','code'=>'HT'],
            ['name'=>'Honduras','code'=>'HN'],
            ['name'=>'Jamaica','code'=>'JM'],
            ['name'=>'México ','code'=>'MX'],
            ['name'=>'Nicaragua','code'=>'NI'],
            ['name'=>'Panamá','code'=>'PA'],
            ['name'=>'Paraguay','code'=>'PY'],
            ['name'=>'Perú','code'=>'PE'],
            ['name'=>'Puerto Rico','code'=>'PR'],
            ['name'=>'República Dominicana','code'=>'DO'],
            ['name'=>'San Vicente y las Granadinas','code'=>'VC'],
            ['name'=>'Santa Lucía','code'=>'LC'],
            ['name'=>'Surinam','code'=>'SR'],
            ['name'=>'Trinidad y Tobago','code'=>'TT'],
            ['name'=>'Uruguay','code'=>'UY'],
            ['name'=>'Venezuela','code'=>'VE'],
     //Europa:
            ['name'=>'Alemania','code'=>'DE'],
            ['name'=>'España','code'=>'ES'],
            ['name'=>'Rusia','code'=>'RU'],
            ['name'=>'Francia','code'=>'FR'],
            ['name'=>'Italia','code'=>'IT'],
            ['name'=>'Portugal','code'=>'PT'],
            ['name'=>'Reino Unido','code'=>'GB']
        ];

        foreach ($Countrys as $country) {
            \App\country::create($country);
        }
    }
}
