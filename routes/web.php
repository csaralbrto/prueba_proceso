<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*rutas que no necesitan estar logueado*/
Route::get('/','HomeController@viewHome')->name('main');
Route::get('/RegisterUser', 'userController@viewRegister')->name('RegisterUser');
Route::get('/Password_Forgot', function () {return view ('forgotPassword');})->name('forgotPassword');
Route::post('/RegisterUser', 'userController@register');
Route::post('/mail_forgot', 'userController@sendmail');
Route::get('/Password_Reset/{token}', function($token) {return view('resetPassword',compact('token'));})->name('resetPassword');

/*login personalizado permite verificar suscripcion*/
Route::post('/login2', 'userController@loginWeb')->name('login2');


/*rutas que necesitan estar autenticado*/
Route::middleware(['auth'])->group(function () { 
  Route::get('/ChangePassword', function () {return view ('changePassword');})->name('ChangePassword');
  Route::post('/ChangePassword', 'userController@changePassword');

  /*proceso*/
  Route::get('/process', 'processController@viewProcess')->name('process');
  Route::get('/process_datatable', 'processController@datatableProcess')->name('process_datatable');

  /*Creación*/
  Route::post('/process', 'processController@Create_process');

  /*Eliminacio*/
  Route::post('/delete_process', 'processController@deleteProcess');

   /*Actualizacion*/
   Route::post('/update_process', 'processController@updateProcess');


});

Route::get('/home', 'HomeController@index')->name('welcome');
Auth::routes();


