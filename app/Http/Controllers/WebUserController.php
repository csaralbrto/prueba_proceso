<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\User;
use Illuminate\Support\Facades\Hash;
use Datatables;
use Mail;
class WebUserController extends Controller
{
        /*Autntificacion*/

    public function loginWeb (Request $request) {
      Auth::logout();
      $email = $request->input('email');
      $password = $request->input('password');
      if (Auth::attempt(['email' => $email, 'password' => $password], true)) {
       $user=Auth::user();
       Session::save();
        return redirect()->route('home');
      } else {
      	return redirect('/')->with('status', 'Usuario no encontrado!');
      }
    }

    public function logoutWeb () {
      Auth::logout();
      return redirect()->route('home');
    }

    public function register(Request $request) {
      $email = $request->input('email');
      $user_email = User::where('email','=',$email)->first();

      if ($user_email) {
        abort('409','Email se encuentra registrado');
      }

      $new_user = new User();
      $new_user->fill($request->all());
      $new_user->password= Hash::make($request->input('password'));
      $new_user->role_id ="2";
      $new_user->save();
 
      return $new_user;
    }

    /*cambiar contrase;a*/
    public function ChangePassword (Request $request, $id) {
      $user = User::findOrfail($id);
      $user->password= Hash::make($request->input('password'));
      $user->save();
      return $user;
    }

}
