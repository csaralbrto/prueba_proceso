<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Mail;
use Session;
use Redirect;
use Mailgun\Mailgun;
use Carbon\Carbon;

class userController extends Controller
{

    /*Muestra ventana de registrar usuario*/
    public function viewRegister () {
      return view ('register');
    }
    /*Funcion de Validar el login del usuario, si ese existe o no*/
    public function loginWeb (Request $request) {
      $email = $request->input('email');
      $password = $request->input('password');
      if (Auth::attempt(['email' => $email, 'password' => $password], false)) {
        $user = Auth::user();
        return redirect('/process');
      } else {
        return redirect('/login')->with('status', 'Usuario no encontrado!');
      }
    }
     /* Funcion de Registrar Usuario*/
     public function register(Request $request) {
      $email = $request->input('email');
      $user_email = User::where('email','=',$email)->first();

      if ($user_email) {
        abort('409','Email se encuentra registrado');
      }

      $new_user = new User();
      $new_user->fill($request->all());
      $new_user->password= Hash::make($request->input('password'));
      $new_user->role_id ="1";
      $new_user->save();
      return $new_user;
    }

    /*Cambio de contarseña de usuario*/
    public function changePassword(Request $request) {
      $user = Auth::user();
      $user->password = Hash::make($request->input('password'));
      $user->save();
      return $user;
    }

    /*email de olvido de contrase;a*/
    public function sendmail(Request $request)  {
      $email = $request->input('email');

      $user = User::where('email', '=', $email)->first();

      if (!$user) {
        abort(409, "El usuario no se encuentra registrado");
      }

      $email = [];
      Mail::send('emails.contact', $email, function($msj){
        $msj->to($email)->subject('Recuperar Contraseña');
      });

      return "Enviado correctamente";
    }
}
