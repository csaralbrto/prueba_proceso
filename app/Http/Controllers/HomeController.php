<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\process;
use App\magazine;
use App\article;
use App\blog;
use Mail;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function viewHome () {
      $process =  process::orderBy('created_at','asc')->get();
      return view ('home', compact('process'));
    }
}
