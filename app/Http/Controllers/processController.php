<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use App\process;
use Carbon\Carbon;
use App\country;
use Illuminate\Support\Facades\Auth;
use DB;
use File;


class processController extends Controller
{
	/*retorna vista de process*/
    public function viewProcess () {

      $user = process::join('users', 'process.id_user', '=', 'users.id');

      if (Auth::user()->role_id!=1) {
        return redirect('/');
      }
    	return view ('process.process', compact('user'));
    }
    /*datatable del proceso*/
    public function datatableProcess () {
    	$process = process::all();
    	return datatables()->of($process)->toJson();
    } 

    /*crear un proceso*/
    public function Create_process(Request $request) {
        $id = Auth::id();
        $process = new process();
        $process->fill($request->all());
        $process->id_user=$id;
        $process->save();
      return $process;
    }
    /* - Eliminar Proceso - */
    public function deleteProcess (Request $request) {  
       $process_id = $request->input('process_id');
       $process = process::findOrFail($process_id);
       $process->delete();
       return "Eliminado";
    }

    /* - Actualizar Proceso - */
    public function updateProcess (Request $request) {
      $process_id = $request->input('process_id');
      $process = process::findOrFail($process_id);
      $process->fill($request->all());
      $process->delete();
      $process->save();
      return $process;
    }
}
