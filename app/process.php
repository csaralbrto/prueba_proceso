<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class process extends Model
{
    protected $table = 'process';
    protected $fillable = [
    	'numprocess',
    	'id_user',
        'description',
        'site',
        'budget'
    ];
}