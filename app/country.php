<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class country extends Model
{
	protected $table = 'country';
    protected $primaryKey = 'code';
    public $incrementing = false;
    protected $fillable = [
        'code',
        'name',
        'description'
    ];
}
