<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Notifications\ResetPassword as ResetNotification;
use Mail;
use App\User;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password', 
        'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token){
          $email = [];
          $email['token'] = $token;
          $user = User::where('email','=', request()->email)->first();
          $email['name']= $user->name;
          $email['surname']=$user->surname;

          Mail::send('emails.password', $email, function($msj){
            $msj->to(request()->email)->subject('Recuperar Contraseña');
          });
    }
}
